package be.foreach.bitbucket.hook;

import com.atlassian.bitbucket.auth.AuthenticationContext;
import com.atlassian.bitbucket.hook.repository.AsyncPostReceiveRepositoryHook;
import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.mail.MailSizeExceededException;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.git.command.GitCommand;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.bitbucket.setting.RepositorySettingsValidator;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.setting.SettingsValidationErrors;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.velocity.exception.VelocityException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class PushEmailNotifier implements AsyncPostReceiveRepositoryHook, RepositorySettingsValidator {
    private static final Logger LOG = LoggerFactory.getLogger(PushEmailNotifier.class);

    private final SmtpNotificationRenderer smtpNotificationRenderer;
    private final AuthenticationContext authenticationContext;
    private final ApplicationPropertiesService applicationPropertiesService;
    private final GitCommandBuilderFactory gitCommandBuilderFactory;
    private final TemplateRenderer templateRenderer;

    private final String defaultTemplate;
    private final String defaultFromTemplate;
    private final String defaultSubjectTemplate;
    private final String defaultSlackTemplate;

    public PushEmailNotifier(final SmtpNotificationRenderer smtpNotificationRenderer, final AuthenticationContext authenticationContext,
                             final ApplicationPropertiesService applicationPropertiesService, final GitCommandBuilderFactory gitCommandBuilderFactory,
                             final TemplateRenderer templateRenderer) throws IOException {
        this.smtpNotificationRenderer = smtpNotificationRenderer;
        this.authenticationContext = authenticationContext;
        this.applicationPropertiesService = applicationPropertiesService;
        this.gitCommandBuilderFactory = gitCommandBuilderFactory;
        this.templateRenderer = templateRenderer;

        defaultTemplate = getResource("notifyemail.vm");
        defaultFromTemplate = getResource("notifyemailfrom.vm");
        defaultSubjectTemplate = getResource("notifyemailsubject.vm");
        defaultSlackTemplate = getResource("notifyslack.vm");
    }

    public static String readTextFile(InputStream is, String encoding) throws IOException {
        StringBuilder sb = new StringBuilder(1024);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(is, encoding));

            int reads = 0;
            char[] chars = new char[1024];
            int numRead;
            while ((numRead = reader.read(chars)) > -1) {
                if (reads++ == 0 && (int) chars[0] == 65279) {
                    // Skip BOM character
                    sb.append(String.valueOf(chars, 1, numRead - 1));
                } else {
                    sb.append(String.valueOf(chars, 0, numRead));
                }
            }
        } finally {
            IOUtils.closeQuietly(reader);
        }

        return sb.toString();
    }

    private String getResource(String templateName) throws IOException {
        InputStream is = PushEmailNotifier.class.getClassLoader().getResourceAsStream("templates/" + templateName);
        if (is != null) {
            return readTextFile(is, "UTF-8");
        } else {
            return "Could not find resource";
        }
    }

    @Override
    public void postReceive(@Nonnull RepositoryHookContext context, @Nonnull Collection<RefChange> refChanges) {
        LOG.debug("refchanges size:" + refChanges.size());
        String mailTo = context.getSettings().getString(NotificationSettings.FIELD_MAIL_TO);
        String slackChannel = context.getSettings().getString(NotificationSettings.FIELD_SLACK_CHANNELS);

        List<String> emailAddresses = splitAndTrimEmails(mailTo);
        List<String> slackChannels = splitAndTrim(slackChannel);
        Repository repository = context.getRepository();
        for (RefChange refChange : refChanges) {
            if (!testRefChange(context.getSettings(), refChange)) {
                Map<String, Object> renderContext = getRenderContext(repository, refChange);
                if (!slackChannels.isEmpty()) {
                    sendSlackNotification(renderContext, slackChannels, repository, context.getSettings());
                }

                sendEmailForRefChange(renderContext, emailAddresses, refChange, context.getSettings());
            } else {
                LOG.info("Skipping refChange on {} because it is excluded", refChange.getRef().getId());
            }
        }
    }

    private Map<String, Object> getRenderContext(Repository repository, RefChange refChange) {
        Map<String, Object> context = new HashMap<>();
        context.put("name", "World");
        context.put("repository", repository);
        context.put("refChange", refChange);
        context.put("currentUser", authenticationContext.getCurrentUser());
        context.put("smtpNotificationRenderer", smtpNotificationRenderer);
        context.put("stringEscapeUtils", new StringEscapeUtils());
        context.put("applicationProperties", applicationPropertiesService);
        context.put("serverDomain", getServerDomain());
        if (!smtpNotificationRenderer.isDeletedBranch(refChange) && !smtpNotificationRenderer.isCreatedBranch(refChange)) {
            // We're not really interested in the number of commits on deleted branches
            context.put("realNumberOfCommits", getRealNumberOfCommits(repository, refChange));
        }

        return context;
    }

    private boolean testRefChange(@Nonnull Settings settings, RefChange refChange) {
        // tests branche if should be included for notification
        String branchFilter = settings.getString(NotificationSettings.FIELD_BRANCH_FILTER);
        List<String> testBranches = splitAndTrim(branchFilter);

        boolean excludeBranchNotification = false;
        boolean includeStringExist = false;

        for (String testBranch : testBranches) {
            if (testBranch.length() <= 1)
                continue;
            if (testBranch.substring(0, 1).equals('+')) {
                includeStringExist = true;
                if (!excludeBranchNotification && StringUtils.contains(refChange.getRef().getId(), testBranch.substring(1))) {
                    excludeBranchNotification = true;
                }
            }
            else if (testBranch.substring(0, 1).equals('-')) {
                if (StringUtils.contains(refChange.getRef().getId(), testBranch.substring(1))) {
                    return false;
                }
            }
        }
        
        if (excludeBranchNotification || !includeStringExist)
            return true;

        // Check for excluded operations
        if (settings.getBoolean(NotificationSettings.DELETE_BRANCH_OPERATION, false))
        {
            if (smtpNotificationRenderer.isDeletedBranch(refChange) && !smtpNotificationRenderer.isTag(refChange))
                return true;
        }
        if (settings.getBoolean(NotificationSettings.DELETE_TAG_OPERATION, false))
        {
            if (smtpNotificationRenderer.isDeletedBranch(refChange) && smtpNotificationRenderer.isTag(refChange))
                return true;
        }

        return false;
    }
    
    private void sendSlackNotification(Map<String, Object> context, List<String> slackChannels, Repository repository, Settings settings) {
        CloseableHttpClient httpclient = null;
        try {

            // Render the template and strip out
            String text = StringUtils.replaceChars(renderTemplate(context, defaultSlackTemplate, null), "\r\n", "").replaceAll("( ){2,}", " ");
            // Replace our json friendly new lines
            text = text.replace(SmtpNotificationRenderer.FRIENDLY_NEWLINE, "\\n");
            // Remove the last trailing newline
            StringUtils.stripEnd(text, "\\n");

            for (String channel : slackChannels) {
                httpclient = HttpClientBuilder.create().build();
                HttpPost post = new HttpPost(settings.getString(NotificationSettings.FIELD_SLACK_WEB_HOOK_URL));

                List<NameValuePair> nvps = new ArrayList<>();
                final String requestBody = toValidJson("{\"channel\": \"" + channel + "\", \"username\": \"" + repository.getName() + " \", \"text\": \"" + text + "\" }");
                if (requestBody != null) {
                    nvps.add(new BasicNameValuePair("payload", requestBody));

                    post.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));

                    ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
                        public String handleResponse(
                                final HttpResponse response) throws IOException {
                            int status = response.getStatusLine().getStatusCode();
                            HttpEntity entity = response.getEntity();
                            if (status >= 200 && status < 300) {
                                return entity != null ? EntityUtils.toString(entity) : null;
                            } else {
                                LOG.error("Bad response status {}, body: {}, payload: {}", status, (entity != null ? EntityUtils.toString(entity) : "NO BODY"), requestBody);
                                throw new ClientProtocolException("Unexpected response status: " + status);
                            }
                        }

                    };
                    String responseBody = httpclient.execute(post, responseHandler);
                    LOG.info(responseBody);
                } else {
                    LOG.error("*** Skipping slack notification because invalid payload");
                }
            }
        } catch (Exception e) {
            LOG.error("Exception talking to slack", e);
        } finally {
            if (httpclient != null) {
                try {
                    httpclient.close();
                } catch (IOException e) {
                    LOG.error("Exception closing httpclient", e);
                }
            }
        }
    }

    public String toValidJson(String test) {
        String result;
        try {
            result = new JSONObject(test).toString();
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                result = new JSONArray(test).toString();
            } catch (JSONException ex1) {
                LOG.error("Not a valid json: ", ex1);
                return null;
            }
        }
        return result;
    }

    private void sendEmailForRefChange(Map<String, Object> context, List<String> emailAddresses, RefChange refChange, Settings settings) {
        LOG.info("Checking ref:{}, type:{}, from:{}, to:{}", refChange.getRef().getId(), refChange.getType(), refChange.getFromHash(), refChange.getToHash());
        Boolean plainTextEmail = settings.getBoolean(NotificationSettings.FIELD_PLAINTEXT_EMAIL);
        try {
            renderAndSendMail(context, emailAddresses, refChange, false, settings, plainTextEmail);
        } catch (MailSizeExceededException ex) {
            // Try sending a light email
            try {
                renderAndSendMail(context, emailAddresses, refChange, true, settings, plainTextEmail);
            } catch (Throwable t) {
                // When sending a light mail fails, send a general error mail
                sendEmail(emailAddresses, "Bitbucket", "Error sending mail for refChange: " + getSubjectPrefix(refChange), t.getMessage(), plainTextEmail);
            }
        }
    }

    private void renderAndSendMail(Map<String, Object> context, List<String> emailAddresses, RefChange refChange, boolean renderLightEmail, Settings settings, Boolean plainTextEmail) {
        String mailBody;
        String fromName;
        String subject;
        try {
            context.put("renderLightEmail", renderLightEmail);

            mailBody = renderTemplate(context, defaultTemplate, getOverriddenTemplate(settings, NotificationSettings.FIELD_MAIL_OVERRIDES_BODY, NotificationSettings.TEMPLATE_BODY));
            fromName = renderTemplate(context, defaultFromTemplate, getOverriddenTemplate(settings, NotificationSettings.FIELD_MAIL_OVERRIDES_FROM, NotificationSettings.TEMPLATE_FROM));
            subject = getSubject(context, settings);
        } catch (Throwable t) {
            String fullStack = ExceptionUtils.getStackTrace(t);
            mailBody = "<pre>" + t.getMessage() + "\r\n" + fullStack + "</pre>";
            fromName = "notifier@" + getServerDomain();
            subject = getSubjectPrefix(refChange);
        }
        sendEmail(emailAddresses, fromName, subject, mailBody, plainTextEmail );
    }

    private String getSubject(Map<String, Object> context, Settings settings) throws IOException {
        return StringUtils.replaceChars(
                renderTemplate(
                        context, defaultSubjectTemplate,
                        getOverriddenTemplate(
                                settings, NotificationSettings.FIELD_MAIL_OVERRIDES_SUBJECT, NotificationSettings.TEMPLATE_SUBJECT
                        )
                ), "\r\n", "").replaceAll("( ){2,}",
                " "
        );
    }

    private String renderTemplate(Map<String, Object> context, String template, String overridenTemplate) throws IOException {
        String mailBody = this.templateRenderer.renderFragment(StringUtils.isNotEmpty(overridenTemplate) ? overridenTemplate : template, context);
        mailBody = StringUtils.trim(smtpNotificationRenderer.stripWhiteSpacesToNewLines(mailBody));
        return mailBody;
    }

    private void sendEmail(List<String> emailAddresses, String fromName, String subject, String mailBody, Boolean plainTextEmail) {
        smtpNotificationRenderer.sendMail(emailAddresses, fromName, subject, mailBody, plainTextEmail);
    }

    private int getRealNumberOfCommits(Repository repository, RefChange refChange) {
        GitStringOutputHandler stringOutputHandler = new GitStringOutputHandler();
        GitCommand<String> gitCommand = gitCommandBuilderFactory.builder(repository).command("rev-list").argument(refChange.getFromHash() + ".." + refChange.getToHash()).argument("--count").build(stringOutputHandler);
        String output = gitCommand.call();
        if (output != null) {
            return Integer.parseInt(output.replaceAll("(\\t|\\r?\\n)+", ""));
        }
        return 0;
    }

    private String getSubjectPrefix(RefChange refChange) {
        return "[" + smtpNotificationRenderer.getShortHash(refChange.getFromHash()) + ".." + smtpNotificationRenderer.getShortHash(refChange.getToHash()) + "@" + refChange.getRef().getId() + "]";
    }

    private String getServerDomain() {
        String from = applicationPropertiesService.getServerEmailAddress();
        if (from == null) {
            return "localhost.local";
        } else {
            String[] serverDomain = from.split("@");
            return serverDomain.length > 1 ? serverDomain[1] : serverDomain[0];
        }
    }

    public String getOverriddenTemplate(Settings settings, String radio, String template) {
        if (settings != null && settings.getBoolean(radio, false)) {
            return settings.getString(template);
        }
        return null;
    }

    @Override
    public void validate(@Nonnull Settings settings, @Nonnull SettingsValidationErrors errors, @Nonnull Repository repository) {
        if (isSettingEmpty(settings, NotificationSettings.ENABLE_MAIL_NOTIFICATION) && isSettingEmpty(settings, NotificationSettings.ENABLE_SLACK_NOTIFICATION)) {
            errors.addFieldError(NotificationSettings.ERROR_NOTIFICATION_TYPES, "Please enable at least one type of notification");
            return; // no need to validate the rest
        }

        // mail notification validation
        if (!isSettingEmpty(settings, NotificationSettings.ENABLE_MAIL_NOTIFICATION)) {
            if (isSettingEmpty(settings, NotificationSettings.FIELD_MAIL_TO)) {
                errors.addFieldError(NotificationSettings.FIELD_MAIL_TO, "Please provide an email address");
                errors.addFieldError(NotificationSettings.ENABLE_MAIL_NOTIFICATION, "The Mail configuration has errors");
            }

            validateTemplate(settings, errors, NotificationSettings.FIELD_MAIL_OVERRIDES_FROM, NotificationSettings.TEMPLATE_FROM);
            validateTemplate(settings, errors, NotificationSettings.FIELD_MAIL_OVERRIDES_SUBJECT, NotificationSettings.TEMPLATE_SUBJECT);
            validateTemplate(settings, errors, NotificationSettings.FIELD_MAIL_OVERRIDES_BODY, NotificationSettings.TEMPLATE_BODY);
        } else {
            if (!isSettingEmpty(settings, NotificationSettings.FIELD_MAIL_TO)) {
                errors.addFieldError(NotificationSettings.FIELD_MAIL_TO, "Please remove the email address");
            }
        }

        // slack notification validation
        if (!isSettingEmpty(settings, NotificationSettings.ENABLE_SLACK_NOTIFICATION)) {
            boolean hasErrors = false;
            if (isSettingEmpty(settings, NotificationSettings.FIELD_SLACK_WEB_HOOK_URL)) {
                errors.addFieldError(NotificationSettings.FIELD_SLACK_WEB_HOOK_URL, "Please provide a Slack web hook URL");
                hasErrors = true;
            }

            if (isSettingEmpty(settings, NotificationSettings.FIELD_SLACK_CHANNELS)) {
                errors.addFieldError(NotificationSettings.FIELD_SLACK_CHANNELS, "Please provide a #channel and/or @username to notify");
                hasErrors = true;
            }
            if (hasErrors) {
                errors.addFieldError(NotificationSettings.ENABLE_SLACK_NOTIFICATION, "The Slack configuration has errors");
            }
        }
    }

    private boolean isSettingEmpty(Settings settings, String setting) {
        return settings.getString(setting, "").isEmpty();
    }

    private void validateTemplate(Settings settings, SettingsValidationErrors errors, String overrideTemplateRadio, String mailTemplate) {
        boolean overrideTemplate = settings.getBoolean(overrideTemplateRadio, false);

        if (overrideTemplate) {
            String mailTemplateSettings = settings.getString(mailTemplate, "");
            if (mailTemplateSettings.isEmpty()) {
                errors.addFieldError(mailTemplate, "Please provide a template if you wish to override it");
            } else {
                try {
                    renderTemplate(new HashMap<>(), mailTemplateSettings, mailTemplateSettings);
                } catch (VelocityException e) {
                    errors.addFieldError(mailTemplate, e.getMessage());
                } catch (IOException io) {
                    errors.addFieldError(mailTemplate, "Cannot evaluate template" + io.getMessage());
                }
            }
        }
    }

    private List<String> splitAndTrim(String text) {
        List<String> items = new ArrayList<>();
        if (StringUtils.isNotEmpty(text)) {
            if (text.indexOf(';') >= 0) {
                String[] splitItems = text.split(";");
                for (String splitItem : splitItems) {
                    items.add(StringUtils.trim(splitItem));
                }
            } else {
                items.add(StringUtils.trim(text));
            }
        }
        return items;
    }

    private List<String> splitAndTrimEmails(String text) {
        List<String> items = new ArrayList<>();
        if (StringUtils.isNotEmpty(text)) {
            String[] splitItems = text.split("[;,]");
            for (String splitItem : splitItems) {
                items.add(StringUtils.trim(splitItem));
            }
        }
        return items;
    }
}
