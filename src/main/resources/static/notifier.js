define('notifier/notifier', [
    'exports',
    'jquery'
], function( exports, $ ) {

    function capitalize(input) {
        return input.charAt(0).toUpperCase() + input.slice(1);
    }

    function toggleState( node, checked ) {
        if (checked) {
            node.removeAttr('readonly');
        } else {
            node.attr('readonly', 'readonly');
        }
    }

    exports.onReady = function() {
        // initialize mailing template override state toggles
        ['from', 'subject', 'body'].map(function(field) {
            var ok = $('#override-' + field + '-yes');
            ok.change(function() {
                toggleState($('#mail' + capitalize(field) + 'Template'), $(this).is(':checked'));
            });
            $('#override-' + field + '-nok').change(function() {
                toggleState($('#mail' + capitalize(field) + 'Template'), $(this).is(':checked'));
            });
            ok.trigger('change');
        });
        // initialize message type state toggles
        ['mail', 'slack'].map(function(type) {
            var nType = $('#' + type + 'Notification');
            nType.change(function() {
                $('#' + type + 'Section').toggleClass('hidden', !$(this).is(':checked'));
            });
            nType.trigger('change');
        });
    };
});
